FROM python:3.7

#Declaring environment
ENV APP_HOME /app

#Setting Working Directory
WORKDIR $APP_HOME

# Install poetry.
RUN curl -sSL https://install.python-poetry.org | python3 -

#Exporting PATH
RUN export PATH="/root/.local/share/pypoetry/venv/bin:$PATH"

# Copy local code to the container image.
COPY . $APP_HOME

# Install production dependencies.
RUN /root/.local/share/pypoetry/venv/bin/poetry install

# Deactivating creation of virtualenv
RUN /root/.local/share/pypoetry/venv/bin/poetry config virtualenvs.create false


#Install Flask
RUN pip3 install -U flask

EXPOSE 5000

CMD [ "python3","manage.py"] 