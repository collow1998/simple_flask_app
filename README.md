# simple_flask_app

# Notes

- I have used branch as master however we can always change it to dev,pre-prod,prod and feature-branches or hot-fix respective to the environment that we want to deploy to.

- Merge requests would be raised to move to a higher branch.

- I have disabled approval to production. Also the different environments can be configured to different stages.

- CD Part would need a server to implement however the job is already created.For examle on cloud run(GCP):
        deploy:
        stage: deploy
        needs: [test]
        only:
            - master # This pipeline stage will run on this branch alone
        image: google/cloud-sdk
        services:
            - docker:dind
        script:
            - echo $GCP_SERVICE_KEY > gcloud-service-key.json # Save Google cloud contents in a temporary json file
            - gcloud auth activate-service-account --key-file gcloud-service-key.json # Activate your service account
            - gcloud auth configure-docker # Configure docker environment
            - gcloud config set project $GCP_PROJECT_ID #Set the GCP Project ID to the variable name
            - gcloud builds submit --tag gcr.io/$GCP_PROJECT_ID/$SERVICE_NAME #Run the gcloud build command to build our image
            - gcloud run deploy $SERVICE_NAME --image gcr.io/$GCP_PROJECT_ID/$SERVICE_NAME --region=us-east4 --platform managed --allow-unauthenticated # Run the gcloud run deploy command to deploy our new service


- docker image is on dockerhub: https://hub.docker.com/repository/docker/collow1998/simple_flask_app  
    - docker pull collow1998/simple_flask_app:latest
